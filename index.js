const {Builder, By, Key, util} = require("selenium-webdriver");

const mail = YOUR_FACEBOOK_MAIL_HERE;
const pass = YOUR_FACEBOOK_PASSWORD_HERE;
const COUNT = HOW_MANY_TIMES_YOU_WANT_TO_ITERATE_PROCESS;

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function login(driver) {
    // Google Facebook and login
    await driver.get("https://www.facebook.com/");
    let loginPath = "//*[@id=\"email\"]";
    let passPath = "//*[@id=\"pass\"]";
    let loginEnterBtnPath = "//*[@id=\"loginbutton\"]";
    await driver.findElement(By.xpath(loginPath))
        .sendKeys(mail);

    await driver.findElement(By.xpath(passPath))
        .sendKeys(pass);

    await driver.findElement(By.xpath(loginEnterBtnPath))
        .click();

    // Waiting for Facebook login completion
    await sleep(3000);

    // Google tinder
    await driver.get("https://tinder.com/?lang=ru");

    // Accept privacy polices
    let policyBtnPath = "//*[@id=\"content\"]/div/div[2]/div/div/div[1]/button";
    await driver.findElement(By.xpath(policyBtnPath))
        .click();

    // Waiting for login button and bush it
    await sleep(3000);

    try {
        if (await driver.findElement(By.xpath("//button[text()='Другие варианты']")).isEnabled()) {
            await driver.findElement(By.xpath("//button[text()='Другие варианты']"))
                .click();
        }
    } catch (e) {
    }

    let loginBtnPath = "//button[@aria-label='Войти через Facebook']";
    await driver.findElement(By.xpath(loginBtnPath))
        .click();

    await sleep(3000);
}

async function processMatches(driver) {
    // Allow location and disable notifications
    try {
        await driver.findElement(By.xpath("//*[@role='dialog']//button[@aria-label='Разрешить']"))
            .click();

        await driver.findElement(By.xpath("//*[@role='dialog']//button[@aria-label='Неинтересно']"))
            .click();
    } catch (e) {
    }

    await sleep(3000);

    // Find and click top person
    let topPerson = "document.getElementById('matchListNoMessages').getElementsByClassName('D(f) Flw(w)')[0].childNodes[1].children[0].click()";
    driver.executeScript(topPerson);

    await sleep(4000);

    // Enter message text and send it
    let messageTextPath = "//*[@id=\"chat-text-area\"]";
    let sendMessageBtnPath = "//*[@id=\"content\"]/div/div[1]/div/main/div[1]/div/div/div/div[1]/div/div/div[3]/form/button";

    await driver.findElement(By.xpath(messageTextPath))
        .click();

    await driver.findElement(By.xpath(messageTextPath))
        .sendKeys("Привет, хочешь принять участие в моем эксперименте? Опрос абсолютно безболезненный и анонимный, нужно ответить на вопрос. Для чего ты используешь тиндер? Какая у тебя цель? Мне нужно классифицировать поиск");

    await driver.findElement(By.xpath(sendMessageBtnPath))
        .click();

    await sleep(2000);

    await driver.get("https://tinder.com/app/recs");
}

async function start() {
    let driver = await new Builder()
        .forBrowser("chrome")
        .build();

    try {
        await login(driver);
        for (let i = 0; i < COUNT; i++) {
            await processMatches(driver);
            await sleep(4000);
        }
    } finally {
        await driver.quit();
    }
}

start();

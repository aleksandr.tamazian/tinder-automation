# Tinder automation

### Stack
1. NodeJs
2. [Selenium](https://www.selenium.dev/documentation/en/webdriver/) for JavaScript
3. Russian Tinder audience

### How it's works
First of all, you must have Facebook account creeds in Tinder.

1. Login in Facebook
2. Login in Tinder
3. Click on top person
4. Write message
5. Send

Video available [here](https://drive.google.com/file/d/1av_ynu2wOE7j0h3Nbd2xbQrALD1gs8UA/view?usp=sharing)

### How to use
Before start, set `mail`, `pass` (Facebook accounts) and `COUNT` variables.

But be careful with `COUNT` value, check [spam rules](https://policies.tinder.com/terms/intl/en). 

Then use:
```
node index
```
### TODO
1. Data analysis of results
